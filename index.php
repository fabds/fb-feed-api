<?php

require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

/**
 * FB App Token
 */
$appId = '201332797011047';
/**
 * FB App Secret
 */
$appSecret = 'eb3ee9008beaa35c486f177f049c9562';
/**
 * FB Api Version
 */
$appVersion = 'v3.3';
/**
 * Token senza scadenza
 */
$token = 'EAAC3HHHY9GcBALAAYCmCmC159BneVJfGBtKcjwHPddmcSNLUZAILlJo1jx6sWaSLmTdHdyW2Ovh6rVCX6J1ZCRvVsBpNCwrLDYj5cIdqlAf5AzoRp9jaSXvdo0KEF3W8BTQXrL3UpNxTy4cMmrkpzaZCYZC3UfzpGnsR4sCudtBvcns8TDcRU60ZCZCIHRwHwZD';

/**
 * Init FB Object
 */
$fb = new \Facebook\Facebook([
    'app_id' => $appId,
    'app_secret' => $appSecret,
    'default_graph_version' => $appVersion,
    'default_access_token' => $token
]);

$response = $fb->get('/me');
$me = $response->getGraphUser();
echo '<h1>' . $me->getName() . '</h1>';

try {
    // Facebook Feed
    $response = $fb->get(
        '/me/feed?limit=5&fields=id,message,created_time,full_picture'
    );
    $graphEdge = $response->getGraphEdge();
    echo '<pre>';

    $count = 1;

    foreach ($graphEdge as $edge) {
        echo renderPost($edge->asArray(),$count);
    }

    $graphEdge = $fb->next($graphEdge);

    foreach ($graphEdge as $edge) {
        echo renderPost($edge->asArray(),$count);
    }

} catch(Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}


/**
 * @param $el
 * @param $count
 * @return string
 */
function renderPost($el,&$count){
    ob_start();
    ?>
        <div class="post">
            <?php
            if(isset($el['message'])){
                ?>
                <h2><?php echo $count.' - '.$el['message']; ?></h2>
                <?php
            }

            if(isset($el['created_time'])){
                ?>
                <h3 style="color:red"><?php echo (string)$el['created_time']->format('Y-m-d H:i:s'); ?></h3>
                <?php
            }

            if(isset($el['full_picture'])){
                ?>
                    <img src="<?php echo $el['full_picture']; ?>" />
                <?php
            }
            ?>
        </div>
    <?php
    $count++;
    print_r($el);
    echo "<hr/>";
    return ob_get_clean();
}
